<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//proses generate app key
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

//fungsi register
$router->post('/register', 'AuthController@register');

//fungsi login
$router->post('/login', 'AuthController@login');

// Matches "/profile
$router->get('profile', 'UserController@profile');

// Matches "/users/1 
//mendapatkan user berdasarkan id
$router->get('users/{id}', 'UserController@singleUser');

// Matches "/users
$router->get('users', 'UserController@allUsers');

//update user berdasar id
$router->put('update/{id}', 'UserController@updateUser');

//hapus user berdasar id
$router->delete('delete/{id}', 'UserController@deleteUser');

//proses insert artikel
$router->post('artikel-insert/{id}', 'UserController@insertArtikel');

//menampilkan artikel berdasar id user
$router->get('userartikel/{id}', 'UserController@userArtikel');

//proses update artikel
$router->put('artikel-update/{id}', 'UserController@updateArtikel');

//proses delete artikel
$router->delete('artikel-delete/{id}', 'UserController@deleteArtikel');

//jumlah artikel yang telah diinput user 
$router->get('artikel-total', 'UserController@totalArtikel');

//semua artikel
$router->get('artikels', 'UserController@allArtikel');