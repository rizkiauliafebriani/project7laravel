<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use  App\User;
use App\Artikel;
use App\Http\Requests;

class UserController extends Controller
{
     /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
        return response()->json(['user' => Auth::user()], 200);
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers()
    {
         return response()->json(['users' =>  User::all()], 200);
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

    public function updateUser($id, Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string',
        ]);

        try {
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');

            $user->save();

            return response()->json(['user' => $user, 'message' => 'Updated'], 201);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'User Update Failed!'], 409);
        }
    }

    public function deleteUser($id)
    {
        try {
            $user = User::find($id);
            $user->delete();

            return response()->json(['message' => 'User Deleted'], 201);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'User Delete Failed!'], 409);
        }
    }

    public function insertArtikel($id, Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|string',
            'isi' => 'required|string',
        ]);

        if (User::find($id)) {
            try {
                $artikel = new Artikel;
                $artikel->id_users = $id;
                $artikel->judul = $request->input('judul');
                $artikel->isi = $request->input('isi');
    
                $artikel->save();
    
                return response()->json([
                    'success' => 1,
                    'message' => 'Artikel berhasil diinsert!'
                ], 201);
            } catch (\Throwable $th) {
                return response()->json([
                    'success' => 0,
                    'message' => 'Artikel gagal diinsert'
                ], 409);
            }
        }
        else {
            return response()->json([
                'success' => 0,
                'message' => 'Artikel gagal diinsert!'
            ], 409);
        }
    }

    public function userArtikel($id, Request $request)
    {
        if (User::find($id)) {
            $artikels = Artikel::select('id', 'judul', 'isi')
                ->where('id_users', $id)->get();

            return response()->json(['artikels' =>  $artikels], 200);
        } else {
            return response()->json(['message' => 'Pengguna tidak ditemukan!'], 409);
        }
    }

    public function updateArtikel($id, Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'judul' => 'required|string',
            'isi' => 'required|string',
        ]);

        // $post = Post::find($id);
        if (Artikel::find($id)) {
            try {
                $artikel = Artikel::find($id);
                $artikel->judul = $request->input('judul');
                $artikel->isi = $request->input('isi');
    
                $artikel->update();
    
                return response()->json([
                    'success' => 1,
                    'artikel' => $artikel, 
                    'message' => 'Artikel berhasil di update'
                ], 201);
            } catch (\Throwable $th) {
                return response()->json([
                    'success' => 0,
                    'message' => 'Artikel gagal di update'
                ], 409);
            }
        } else {
            return response()->json([
                'success' => 0,
                'message' => 'Artikel gagal di update'
            ], 409);
        }
    }

    public function deleteArtikel($id)
    {
        if (Artikel::find($id)) {
            try {
                $artikel = Artikel::find($id);
                $artikel->delete();
    
                return response()->json([
                    'success' => 1,
                    'message' => 'Artikel berhasil dihapus'
                ], 201);
            } catch (\Throwable $th) {
                return response()->json([
                    'success' => 0,
                    'message' => 'Artikel gagal dihapus'
                ], 409);
            }
        } else {
            return response()->json([
                'success' => 0,
                'message' => 'Artikel gagal dihapus'
            ], 409);
        }  
    }

    public function totalArtikel()
    {
        $artikel = DB::table('artikels')
        ->select('users.name', DB::raw('count(artikels.judul) as total'))
        ->join('users', 'users.id', '=', 'artikels.id_users')
        ->groupBy('artikels.id_users')
        ->get();
            
        return response()->json(['artikels' =>  $artikel], 200);
    }

    public function allArtikel()
    {
        $artikels = User::join('artikels', 'users.id', '=', 'artikels.id_users')
            ->select('users.id as id_user', 'artikels.id as id_artikel', 'users.name', 'artikels.judul', 'artikels.isi')
            ->orderBy('users.id')->get();
        return response()->json(['Artikels' =>  $artikels], 200);
    }
}